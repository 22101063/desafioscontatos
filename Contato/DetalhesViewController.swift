//
//  DetalhesViewController.swift
//  Contato
//
//  Created by Eder Andrade on 31/08/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {

    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControllerDelegate?
    public var contatoDelegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editarContato" {
            let contatoViewController = segue.destination as? ContatoViewController
            contatoViewController?.contato = contato
            contatoViewController?.delegate = self
        }
    }
    
    @IBAction func excluirContatoTap(_ sender: Any) {
        let alert = UIAlertController(title: "Excluir Contato", message: "Deseja realmente excluir o contato", preferredStyle: .alert)
 alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler:  nil)
      alert.addAction(UIAlertAction(title: "Default", style: .default, handler: { (  delegate?.excluirContato(index: index!)
        navigationController?.popViewController(animated: true)) in
    print("Default action")
    let action = UIContextualAction (style: .destructive, title:
"Deletar contato") { [weak self] (_, _, _) in
guard let self = self else {return}
self.numberOfRows -= 1
self.tableView.deleteRows(at: [indexPath], with:
.automatic)
self.tableView.reloadData()
    }
}

extension DetalhesViewController: ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {
        
    }
    
    func editarContato() {
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
        contatoDelegate?.editarContato()
    }
}
